package net.yandex.speller.services.spellservice;
// This source file is generated by Oracle tools.
// Contents may be subject to change.
// For reporting problems, use the following:
// Generated by Oracle JDeveloper 12c 12.2.1.4.0.2248
public class SpellServiceSoap12Client {
    public static void main(String[] args) {
        SpellService spellService = new SpellService();
        SpellServiceSoap spellServiceSoap = spellService.getSpellServiceSoap12();
        // Add your code to call the desired methods.
        ObjectFactory objFactory = new ObjectFactory();
        CheckTextRequest checkTextRequest = objFactory.createCheckTextRequest();
        checkTextRequest.setLang("ru");
        checkTextRequest.setOptions(8);
        checkTextRequest.setFormat("html");
        checkTextRequest.setText("� � ���� �0������ ������");
        CheckTextResponse checkTextResponse = spellServiceSoap.checkText(checkTextRequest);
        for(SpellError error: checkTextResponse.getSpellResult().getError()){
            String message = String.format("Code: %s, word: %s, s: %s", error.getCode(), error.getWord(), error.getS());
            System.out.println(message);
        }
    }
}
